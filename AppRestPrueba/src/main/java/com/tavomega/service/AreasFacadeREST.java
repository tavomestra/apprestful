/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tavomega.service;

import com.tavomega.model.Areas;
import com.tavomega.model.Areas_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author tavom
 */
@Stateless
@Path("/areas")
public class AreasFacadeREST extends AbstractFacade<Areas> {

    @PersistenceContext(unitName = "com.tavomega_AppRestPrueba_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public AreasFacadeREST() {
        super(Areas.class);
    }

     @POST
    @Path("/area")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String registar(Areas entity) {
        if (getAreaNombre(entity.getNombre()) == null) {
            try {
                super.create(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"Ya se registro esta area.\"}";
        }

    }
    
    @PUT
    @Path("/area/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String edit(@PathParam("id") Integer id, Areas entity) {
        if (super.find(id) != null) {
            try {
                super.edit(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"No existe.\"}";
        }
    }

    @DELETE
    @Path("/area/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String remove(@PathParam("id") Integer id) {
        try {
            super.remove(super.find(id));
            return "{\"state\":\"ok\"}";
        } catch (Exception e) {
            return "{\"state\":\"fail\"}";
        }
    }
    
    @GET
    @Path("/area")
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Areas> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("area/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Areas find(@PathParam("id") Integer id) {
       return super.find(id);
       
    }

    @GET
    @Path("/contador")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    /**
     *<h2>Buscar area por nombre</h2>
     * @param nombre
     * @return Ciudad
     */
    public Areas getAreaNombre(String nombre) {
        em = getEntityManager();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Areas> query = qb.createQuery(Areas.class);
            Root<Areas> usuario = query.from(Areas.class);
            Predicate p;
            p = qb.equal(usuario.get(Areas_.nombre), nombre.trim());
            query.where(p);
            List<Areas> result = em.createQuery(query).getResultList();
            if (result.size() < 1) {
                return null;
            }
            return result.get(0);
        } finally {
           
        }

    }

   

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
