/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tavomega.service;

import com.tavomega.model.Activos;
import com.tavomega.model.Asignaciones;
import com.tavomega.model.Asignaciones_;
import static com.tavomega.model.Ciudad_.nombre;
import com.tavomega.model.Personas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author tavom
 */
@Stateless
@Path("asignaciones")
public class AsignacionesFacadeREST extends AbstractFacade<Asignaciones> {

    @PersistenceContext(unitName = "com.tavomega_AppRestPrueba_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public AsignacionesFacadeREST() {
        super(Asignaciones.class);
    }

    @POST
    @Path("/asignacion")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String registar(Asignaciones entity) {
        if(getValidarActivo(entity.getIdActivo()) == null){   
        try {
              super.create(entity);
              return "{\"state\":\"ok\"}";
            } catch (Exception e) {
              return "{\"state\":\"fail\"}";
            }
        }else{
              return "{\"state\":\"Ya este activo fue asignado anteriormente.\"}";
        }

    }
    
    @PUT
    @Path("/asignacion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String edit(@PathParam("id") Integer id, Asignaciones entity) {
        if (super.find(id) != null) {
            try {
                super.edit(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"No existe.\"}";
        }
    }

    @DELETE
    @Path("/asignacion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String remove(@PathParam("id") Integer id) {
        try {
            super.remove(super.find(id));
            return "{\"state\":\"ok\"}";
        } catch (Exception e) {
            return "{\"state\":\"fail\"}";
        }
    }
    
    @GET
    @Path("/asignacion")
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Asignaciones> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("/asignacion/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Asignaciones find(@PathParam("id") Integer id) {
       return super.find(id);
       
    }

    @GET
    @Path("/contador")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    /**
     *<h2>Validar si el activo ya fue asignado</h2>
     * @param idActivo
     * @return Asignaciones
     */
    public Asignaciones getValidarActivo(Activos idActivo) {
        em = getEntityManager();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Asignaciones> query = qb.createQuery(Asignaciones.class);
            Root<Asignaciones> rt = query.from(Asignaciones.class);
            Predicate p;
            p = qb.equal(rt.get(Asignaciones_.idActivo),idActivo);
            query.where(p);
            List<Asignaciones> result = em.createQuery(query).getResultList();
            if (result.size() < 1) {
                return null;
            }
            return result.get(0);
        } finally {
           
        }
    }
    
   

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
