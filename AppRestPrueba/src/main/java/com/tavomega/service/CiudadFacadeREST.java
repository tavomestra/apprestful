/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tavomega.service;

import com.tavomega.model.Ciudad;
import com.tavomega.model.Ciudad_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author tavom
 */
@Stateless
@Path("/ciudades")
public class CiudadFacadeREST extends AbstractFacade<Ciudad> {

    @PersistenceContext(unitName = "com.tavomega_AppRestPrueba_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public CiudadFacadeREST() {
        super(Ciudad.class);
    }

   
    @POST
    @Path("/ciudad")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String registar(Ciudad entity) {
        if (getCiudadNombre(entity.getNombre()) == null) {
            try {
                super.create(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"Ya se registro esta ciudad.\"}";
        }

    }
    
    @PUT
    @Path("/ciudad/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String edit(@PathParam("id") Integer id, Ciudad entity) {
        if (super.find(id) != null) {
            try {
                super.edit(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"No existe.\"}";
        }
    }

    @DELETE
    @Path("/ciudad/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String remove(@PathParam("id") Integer id) {
        try {
            super.remove(super.find(id));
            return "{\"state\":\"ok\"}";
        } catch (Exception e) {
            return "{\"state\":\"fail\"}";
        }
    }
    
    @GET
    @Path("/ciudad")
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Ciudad> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("ciudad/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Ciudad find(@PathParam("id") Integer id) {
       return super.find(id);
       
    }

    @GET
    @Path("/contador")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    /**
     *<h2>Buscar ciudad por nombre</h2>
     * @param nombre
     * @return Ciudad
     */
    public Ciudad getCiudadNombre(String nombre) {
        em = getEntityManager();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Ciudad> query = qb.createQuery(Ciudad.class);
            Root<Ciudad> usuario = query.from(Ciudad.class);
            Predicate p;
            p = qb.equal(usuario.get(Ciudad_.nombre), nombre.trim());
            query.where(p);
            List<Ciudad> result = em.createQuery(query).getResultList();
            if (result.size() < 1) {
                return null;
            }
            return result.get(0);
        } finally {
           
        }

    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
