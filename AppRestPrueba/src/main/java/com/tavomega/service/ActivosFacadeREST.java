/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tavomega.service;

import com.tavomega.model.Activos;
import com.tavomega.model.Activos_;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author tavom
 */
@Stateless
@Path("/activos")
public class ActivosFacadeREST extends AbstractFacade<Activos> {

    @PersistenceContext(unitName = "com.tavomega_AppRestPrueba_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public ActivosFacadeREST() {
        super(Activos.class);
    }
    
      @POST
    @Path("/activo")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String registar(Activos entity) {
            try {
                super.create(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        

    }
    
    @PUT
    @Path("/activo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String edit(@PathParam("id") Integer id, Activos entity) {
        if (super.find(id) != null) {
            try {
                super.edit(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"No existe.\"}";
        }
    }

    @DELETE
    @Path("/activo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String remove(@PathParam("id") Integer id) {
        try {
            super.remove(super.find(id));
            return "{\"state\":\"ok\"}";
        } catch (Exception e) {
            return "{\"state\":\"fail\"}";
        }
    }
    
    @GET
    @Path("/activo")
    @Override
    @Produces({MediaType.APPLICATION_XML})
    public List<Activos> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("/activo/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Activos find(@PathParam("id") Integer id) {
       return super.find(id);
       
    }

    @GET
    @Path("/contador")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    /**
     * Cambia el estado de un activo
     * @param id 
     * @param estado
     * @param fecha
     * @return String
     */
    @PUT
    @Path("/activo/estado/{id}/{estado}/{fecha_modificacion}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String modificarEstado(@PathParam("id") Integer id,@PathParam("estado") String estado,
            @PathParam("fecha_modificacion") String fecha) {
        if (super.find(id) != null) {
            try {
               SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               Date date = formatter.parse(fecha+" 00:00:00");
               
               Activos ac = super.find(id);
               if(date.after(ac.getFechaCompra())){
                   ac.setEstado(estado);
               ac.setFechaBaja(date);
               super.edit(ac);
                return "{\"state\":\"ok\"}";
               }else{
                    return "{\"state\":\"Fecha de modificacion es anterior a la de compra.\"}";
               }
              
            } catch (Exception e) {
                System.err.println(e.getMessage());
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"No existe.\"}";
        }
    }
    
    /**
     *<h2>Buscar activo por tipo</h2>
     * @param tipo
     * @return List
     */
    @GET
    @Path("/activo/tipo/{tipo}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public List<Activos> listarActivosPorTipo(@PathParam("tipo") String tipo) {
        em = getEntityManager();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Activos> query = qb.createQuery(Activos.class);
            Root<Activos> usuario = query.from(Activos.class);
            Predicate p;
            p = qb.equal(usuario.get(Activos_.tipo), tipo.trim());
            query.where(p);
            List<Activos> result = em.createQuery(query).getResultList();
            if (result.size() < 1) {
                return null;
            }
            return result;
        } catch(Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }
    
    /**
     *<h2>Buscar activo por serial</h2>
     * @param serial
     * @return List
     */
    @GET
    @Path("/activo/serial/{serial}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public List<Activos> listarActivosPorSerial(@PathParam("serial") String serial) {
        em = getEntityManager();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Activos> query = qb.createQuery(Activos.class);
            Root<Activos> usuario = query.from(Activos.class);
            Predicate p;
            p = qb.equal(usuario.get(Activos_.serial), serial.trim());
            query.where(p);
            List<Activos> result = em.createQuery(query).getResultList();
            if (result.size() < 1) {
                return null;
            }
            return result;
        } catch(Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }
    
    /**
     *<h2>Buscar activo por fecha</h2>
     * @param desde
     * @param hasta
     * @return List
     */
    @GET
    @Path("/activo/fecha/{desde}&{hasta}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public List<Activos> listarActivosPorFecha(@PathParam("desde") String desde,@PathParam("hasta") String hasta) {
        em = getEntityManager();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = formatter.parse(desde+" 00:00:00");
            Date date2 = formatter.parse(hasta+" 23:59:59");
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Activos> query = qb.createQuery(Activos.class);
            Root<Activos> rt = query.from(Activos.class);
            Predicate p;
            p = qb.between(rt.get(Activos_.fechaCompra),date1,date2);
            query.where(p);
            query.orderBy(qb.asc(rt.get(Activos_.fechaCompra)));
            List<Activos> result = em.createQuery(query).getResultList();
            if (result.size() < 1) {
                return null;
            }
            return result;
        } catch(Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }
   
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
