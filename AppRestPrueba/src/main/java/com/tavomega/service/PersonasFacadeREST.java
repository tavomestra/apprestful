/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tavomega.service;

import com.tavomega.model.Personas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author tavom
 */
@Stateless
@Path("/personas")
public class PersonasFacadeREST extends AbstractFacade<Personas> {

    @PersistenceContext(unitName = "com.tavomega_AppRestPrueba_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public PersonasFacadeREST() {
        super(Personas.class);
    }

    @POST
    @Path("/persona")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String registar(Personas entity) {
            try {
                super.create(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        

    }
    
    @PUT
    @Path("/persona/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String edit(@PathParam("id") Integer id, Personas entity) {
        if (super.find(id) != null) {
            try {
                super.edit(entity);
                return "{\"state\":\"ok\"}";
            } catch (Exception e) {
                return "{\"state\":\"fail\"}";
            }
        } else {
            return "{\"state\":\"No existe.\"}";
        }
    }

    @DELETE
    @Path("/persona/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String remove(@PathParam("id") Integer id) {
        try {
            super.remove(super.find(id));
            return "{\"state\":\"ok\"}";
        } catch (Exception e) {
            return "{\"state\":\"fail\"}";
        }
    }
    
    @GET
    @Path("/persona")
    @Override
    @Produces({MediaType.APPLICATION_XML})
    public List<Personas> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("/persona/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Personas find(@PathParam("id") Integer id) {
       return super.find(id);
       
    }

    @GET
    @Path("/contador")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

   

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
