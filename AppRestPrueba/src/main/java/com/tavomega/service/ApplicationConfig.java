/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tavomega.service;

/**
 *
 * @author tavom
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
 
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class ApplicationConfig extends Application{
 @Override
    public Set<Class<?>> getClasses() {
        
        Set<Class<?>> resources = new java.util.HashSet<>();
        
        System.out.println("REST configuration starting: getClasses()");            
        
       
        resources.add(org.glassfish.jersey.jackson.JacksonFeature.class);
        resources.add(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class);
        resources.add(com.tavomega.provider.MyJacksonJsonProvider.class);
        resources.add(ActivosFacadeREST.class);
        resources.add(AreasFacadeREST.class);
        resources.add(AsignacionesFacadeREST.class);
        resources.add(CiudadFacadeREST.class);
        resources.add(PersonasFacadeREST.class);
        resources.add(NewCrossOriginResourceSharingFilter.class);
       
        
        System.out.println("REST configuration ended successfully.");
        
        return resources;
    }
    
    @Override
    public Set<Object> getSingletons() {
        return Collections.emptySet();
    }
    
    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = new HashMap<>();
        
        
//        properties.put("jersey.config.server.wadl.disableWadl", true);
        
              
        
        return properties;
    }     
}
